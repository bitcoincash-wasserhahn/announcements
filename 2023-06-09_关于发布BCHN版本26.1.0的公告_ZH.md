2023年6月9日

# BCHN v26.1.0版本发布公告

Bitcoin Cash Node (BCHN) 项目在此宣布其小幅版本更新 26.1.0 的发布。

该版本实现了一些修正和效能提升，且提供新的介面功能，其中包括 getpeerinfo 指令下的新参数和 -walletnotify 下的新变量。

我们建议运行旧版本的用户升级到v26.1.0，但并不强制。

查看完整的发布说明，请访问:

https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v26.1.0

该链接中有可执行程序和支持平台的源码，你也可以在BCHN的项目网页进行下载：

https://bitcoincashnode.org

我们希望最新版本能带给你好的体验。让我们一起改善BCH。

BCHN团队
